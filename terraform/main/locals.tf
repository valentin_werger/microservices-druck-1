locals {
  tags = merge(
    var.tags,
    {
      TF-Workspace = var.name
    }
  )
}
